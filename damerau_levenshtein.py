#!/usr/bin/env python

# Damerau-Levenshtein Distance
# Based on the wikipedia references
# Developed by Simeon Mugisha
# v1.0

from sys import argv

def prn (H, li, lj):
    for j in range(-2, lj):
        for i in range(-2, li):
            try:
                print (H[(i, j)]),
            except KeyError:
                print '_',
        print '\n'
    
def damlevindist ():
    A = argv[1]
    B = argv[2]
    A_len = len(A)
    B_len = len(B)

    # infinite distance is the max possible distance
    INF = A_len + B_len

    # make the distance matrix H[-1..a.length][-1..b.length]
    H = {(-2, -2): INF}
    for i in range(-1, A_len):
        H[(i, -2)] = INF
        H[(i, -1)] = i+1
    for j in range(-1, B_len):
        H[(-2, j)] = INF
        H[(-1, j)] = j+1

    # fill in the distance matrix H
    for i in range(A_len):
        for j in range(B_len):
            cost = 0
            if (A[i-1] == B[j-1]):
                DB = j
            else:
                cost = 1
            H[(i,j)] = min ( H[(i-1), (j-1)] + cost,  # substitution
                             H[( i ), (j-1)] + 1,     # insertion
                             H[(i-1), ( j )] + 1,     # deletion
                             H[(i-2), (j-2)] + 1)     # transposition

    return H[(A_len-1, B_len-1)]

print damlevindist()
